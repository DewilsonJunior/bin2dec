$("#input-binario").keydown(function(){
        var binInput = $("#input-binario").val();
        var maxBinInput;

        if(binInput.length >= 6){
            maxBinInput = binInput.substr(0,7);

            $("#input-binario").val(maxBinInput);

            if(binInput.length == 8){
                alert("Um numero de no Máximo 8 caracteres.");
            }
        }

});

$("#input-binario").blur(function(){

    var num_bin = this.value;
    num_bin = num_bin.split("");
    var modificado = false;

    for(var i=0; i < num_bin.length; i++){
        if(num_bin.length > 0 && 
            (num_bin[i] != 0 && num_bin[i] != 1)
        ){
            delete num_bin[i];

            modificado = true;
        }
    }

    if(modificado){
        alert("Só é permitido combinações entre zeros e uns, por favor tente novamente");

        $("#input-binario").val(num_bin.join(""));
    }

});

$("#btn-converter").on("click", function(){

    var binValor = $("#input-binario").val();
    var binLength = binValor.length;

    var decValor = binToDec(binValor, binLength);

    $("#input-decimal").val(decValor);
});

function binToDec(binValor, binLength){

    var contadorBin = binLength;
    var result = 0;

    for(i=0; i < contadorBin; i++){
        var bin = binValor.substr(i,1);

        result = result + (bin * (Math.pow(2,i)));

    }

    return result;
}
